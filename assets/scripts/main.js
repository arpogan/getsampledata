import style from '../styles/styles.scss';

document.addEventListener("DOMContentLoaded", function() {

    const getSampleDataStatus = document.querySelectorAll('.get-sample-data'); 
    if (getSampleDataStatus.length) {
        let dataType = document.querySelector('.data-type');
        let dataBox = document.querySelector('.returned-data');
        let statusBox = document.querySelector('.status-box');

        dataType.addEventListener('change', function() {
            if(this.value == 'xml') {
                getData('../data/response.xml', 'xml', function(data){

                    let xml = data.responseXML;

                    let id = 'id: ' + xml.getElementsByTagName('id')[0].firstChild.nodeValue + '<br>';
                    let name = 'name: ' + xml.getElementsByTagName('name')[0].firstChild.nodeValue + '<br>';
                    let externalId = 'external_id: ' + xml.getElementsByTagName('external_id')[0].firstChild.nodeValue + '<br>';
                    let impressions = 'impressions: ' + xml.getElementsByTagName('impressions')[0].firstChild.nodeValue + '<br>';
                    let clicks = 'clicks: ' + xml.getElementsByTagName('clicks')[0].firstChild.nodeValue + '<br>';

                    let responseText = '<pre>'+id+name+externalId+impressions+clicks+'</pre>';
                    let responseURL = data.responseURL;
                    let status = 'Status: '+data.status + '<br>';

                    dataBox.innerHTML = status + responseText;
                    statusBox.innerHTML = "GET " + responseURL;
                    
                 });
            } else {
                getData('../data/response.json', 'json', function(data){    

                   let responseText = '<pre>'+data.responseText+'</pre>';
                   let responseURL = data.responseURL;
                   let status = 'Status: '+data.status + '<br>';

                   dataBox.innerHTML = status + responseText;
                   statusBox.innerHTML = "GET " + responseURL;
                });
            }

        });
    }
});

function getData(file, type, callback) {
    let dataFile = new XMLHttpRequest();
    dataFile.overrideMimeType('application/'+type);
    dataFile.open('GET', file, true);
    dataFile.onreadystatechange = function() {
        if (dataFile.readyState === 4 && dataFile.status == '200') {
            callback(dataFile);
        }
    }
    dataFile.send(null);
}