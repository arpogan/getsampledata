const {resolve} = require("path");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
 
module.exports = function(env) {
 
  var dev = env.development === true;
 
  return {
 
    entry: "./assets/scripts/main.js",
    output: {
      path: resolve(__dirname, './dist/js'),
      filename: 'bundle.js'
    },
    devtool: dev ? "source-map" : "none",
    module: {
        rules: [
          {
            test:/\.s?[ac]ss$/,
            exclude: /\/node_modules\//,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: [
                  {
                      loader: 'css-loader',
                      options: {   
                          url: false
                      }
                  }, 
                  {
                      loader: 'sass-loader',
                  }
                ]
              })
          },
          {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
              fallback: "style-loader",
              use: "css-loader"
            })
          }
        ]
      },
      plugins: [
        new BrowserSyncPlugin({
          host: 'localhost',
          port: 3000,
          files: [
            "./*.*"
          ],
          server: { baseDir: ['dist'] }
        }),
        new ExtractTextPlugin("../styles/styles.css"), 
        new CopyWebpackPlugin([
        {from: './assets/data',to: resolve(__dirname, './dist/data/')},
        {from: './*.html',to: resolve(__dirname, './dist/')} 
        ]), 
      ]
  };
 
}